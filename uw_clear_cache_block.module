<?php

/**
 * @file
 * Implements a block to add a button to clear all caches.
 */

/**
 * Implements hook_block_info().
 */
function uw_clear_cache_block_block_info() {
  $blocks['clear_all_caches'] = array(
    'info' => t('Clear all caches'),
    'status' => 0,
  );
  return $blocks;
};

/**
 * Implements hook_block_view().
 */
function uw_clear_cache_block_block_view($delta = '') {
  switch ($delta) {
    case 'clear_all_caches':
      $block['subject'] = t('Clear cache');
      $block['content'] = uw_clear_cache_block_contents();
      return $block;
  }
}

/**
 * A module-defined block content function.
 */
function uw_clear_cache_block_contents() {
  if (user_access('access dashboard')) {
    $output = array(
      'first_para' => array(
        '#type' => 'markup',
        '#markup' => '<ul><li><a href="clear-all-caches">Clear all caches</a></li></p>',
      ),
    );
  }
  else {
    $output = array(
      'first_para' => array(
        '#type' => 'markup',
        '#markup' => '<p>Access denied</p>',
      ),
    );
  }
  return $output;
}

/**
 * Implements hook_menu().
 */
function uw_clear_cache_block_menu() {
  $items['admin/clear-all-caches'] = array(
    'title' => 'Clear all caches',
    'type' => MENU_NORMAL_ITEM,
    'page callback' => 'uw_clear_cache',
    'access callback' => 'user_access',
    'access arguments' => array('access dashboard'),
  );
  return $items;
}

/**
 * Implements hook_navbar().
 */
function uw_clear_cache_block_navbar() {
  $items['uw_clear_cache_block'] = array(
    '#attached' => array(
      'css' => array(drupal_get_path('module', 'uw_clear_cache_block') . '/uw_clear_cache_block.navbar.icons.css'),
    ),
  );
  return $items;
}

/**
 * Handle callback.
 */
function uw_clear_cache() {
  drupal_flush_all_caches();
  drupal_set_message(t('All caches cleared at @time.', array('@time' => format_date(time()))));
  // Need to set this so that it doesn't try to access itself.
  drupal_goto('admin/dashboard');
}
